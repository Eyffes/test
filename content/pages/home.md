Title:
URL:
Save_as: index.html
Template: home

Nomadic Labs houses a team focused on Research and Development. Our core
competencies are in programming language theory and practice, distributed
systems, and formal verification.  We believe our strength lies in a unique mix
of skills and experience, allowing us to transfer the best of academic research
into real world applications.

We currently focus on contributing to the development of the [Tezos](https://tezos.com/) core
software, including the smart-contract language, [Michelson](https://tezos.gitlab.io/whitedoc/michelson.html). We also have a
mobile team working on the Cortez wallet ([Android](https://gitlab.com/nomadic-labs/cortez-android), [iOS](https://gitlab.com/nomadic-labs/cortez-ios)).

As a member of the Tezos community, we are fortunate to collaborate daily with
academic institutions and many other contributors to the ecosystem. Some of our
closest collaborators are researchers from the French research institute [Inria](https://www.inria.fr/),
[OCaml Labs](http://ocamllabs.io/) in Cambridge (UK), [Tarides](https://tarides.com/) and [Ligo](https://ligolang.org/) in Paris, [Cryptium Labs](https://cryptium.ch/) in Switzerland, [Obsidian Systems](https://obsidian.systems/), [Cryptonomic](https://cryptonomic.tech/), [TQ-Tezos](https://tqtezos.com/), [camlCase](https://camlcase.io/) and [Runtime Verification](https://runtimeverification.com/) in the United States, [DaiLambda](https://www.dailambda.jp/) in Japan and, last but not least, many
individual developers.
